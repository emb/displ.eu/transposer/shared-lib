import os
import logging
import json
from dotenv import load_dotenv
from requests.exceptions import HTTPError
import langid
from guess_language import guess_language
from langdetect import detect, DetectorFactory
from cerberus import Validator
from base_adapter import BaseAdapter
from collections import Counter


PAYLOAD_SCHEMA = {
    'input': {
        'anyof': [
            {'type': 'dict', 'allow_unknown': True},
            {'type': 'string'}
        ],
        'required': True
    },
    'format': {
        'type': 'string',
        'allowed': [
            'text',
            'html',
            'transcription'
        ]
    }
}

class Adapter(BaseAdapter):
    def __init__(self, connector, config_path):
        super(Adapter, self).__init__(connector, config_path)
        self._validator = Validator(PAYLOAD_SCHEMA)
        self._validator.allow_unknown = True
        print('My Adapter ... config:', self.config)


    def run(self, payload, msg):
        print('My Adapter ... run ... msg:', msg)
        result = {'success': False}

        if isinstance(payload, str):
            try:
                payload = json.loads(payload)
            except:
                logging.error('Unable to decode payload')
                result['message'] = 'Unable to decode payload'
                return result

        if not self._validator.validate(payload):
            logging.error(self._validator.errors)
            result['message'] = str(self._validator.errors)
            return result

        if 'uuid' in payload:
            result['uuid'] = payload['uuid']
        if 'source_id' in payload:
            result['source_id'] = payload['source_id']

        try:
            text = payload['input']
            if isinstance(text, dict):
                text = json.dumps(text)
            
            result['detected_language'] = self.detect_language(text)
            result['success'] = True
            
        except ValueError as e:
            result['message'] = str(e)
        except Exception as e:
            result['message'] = f"Error detecting language: {str(e)}"

        return result

    def detect_language(self, text):
        DetectorFactory.seed = 42
        try:
            lang1 = langid.classify(text)[0]
        except:
            lang1 = None
            
        try:
            lang2 = guess_language(text)
            if lang2 == 'UNKNOWN':
                lang2 = None
        except:
            lang2 = None
            
        try:
            lang3 = detect(text)
        except:
            lang3 = None

        # Count occurrences of each detected language
        langs = [lang for lang in [lang2, lang3, lang1] if lang is not None]
        if not langs:
            raise ValueError("No language could be detected")

        # If only lang1 detected, raise error as it's considered least reliable
        if len(langs) == 1 and lang1 and not (lang2 or lang3):
            raise ValueError("Only least reliable model detected language")

        # Get most common language
        counts = Counter(langs)
        most_common = counts.most_common()
        
        # If there's a clear majority, return it
        if len(most_common) == 1 or most_common[0][1] > most_common[1][1]:
            return most_common[0][0]
        
        # If tie between models, prioritize lang2 and lang3 over lang1
        tied_langs = [lang for lang, count in most_common if count == most_common[0][1]]
        for lang in [lang2, lang3]:
            if lang in tied_langs:
                return lang
                
        return tied_langs[0]

    def adapter_name(self):
        return 'language_detection'