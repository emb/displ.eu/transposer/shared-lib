# Shared Lib

Purpose of this project:
* Documentation
* Template

This project documents information common to all `modules`, `adapters`, `transformers`, `methods` and `routers`. It does not provide code, only the directory structure expected by other services. For that reason, cloning it is only a convenient way of creating these directories and getting some documentation.

*Modules*, *Adapters*, *Transformers* and *Methods*, that can be installed inside their designated directory, can be found in their respective groups in the *Transposer* repository:

* [Modules](https://git.fairkom.net/emb/displ.eu/transposer/modules)
* [Adapters](https://git.fairkom.net/emb/displ.eu/transposer/adapters)
* [Transformers](https://git.fairkom.net/emb/displ.eu/transposer/transformers)
* [Methods](https://git.fairkom.net/emb/displ.eu/transposer/methods)
* [Routers](https://git.fairkom.net/emb/displ.eu/transposer/routers)


## Install

To install *Shared Lib*, clone it alongside the service(s) that should use it.
```bash
git clone https://git.fairkom.net/emb/displ.eu/transposer/shared-lib.git
```

### Modules, Adapters, Transformers, Methods and Routers

To install *Modules*, *Adapters*, *Transformers*, *Methods* or *Routers* into *Shared Lib*, clone the desired component into its designated directory inside *Shared Lib*. The *Services* will automatically add all components' paths to their search-paths, it is thus necessary to install the components' dependencies into the virtual environments of the *Services*.
